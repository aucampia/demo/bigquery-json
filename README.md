# ...

```bash
poetry run coopnorge.demo2206.gcp --help

poetry run coopnorge.demo2206.gcp generate -f ndjson -o data.ndjson
poetry run coopnorge.demo2206.gcp generate -f csv -o data.json.csv
poetry run coopnorge.demo2206.gcp generate -f ndjson -o gs://coopnorge-demoui7q-sandbox-files/data.ndjson
poetry run coopnorge.demo2206.gcp generate -f csv -o gs://coopnorge-demoui7q-sandbox-files/data.json.csv


gsutil cat gs://coopnorge-demoui7q-sandbox-files/data.json


poetry run coopnorge.demo2206.gcp generate -o data.json

```

```
## JSON STRING
bq rm sandbox-aucamp-e332:dailev.json_string
bq load --project_id=sandbox-aucamp-e332 --source_format=CSV sandbox-aucamp-e332:dailev.json_string data.json.csv json_string:STRING


bq load --project_id=sandbox-aucamp-e332 --source_format=CSV sandbox-aucamp-e332:dailev.json_type data.json.csv json_string:JSON
```

```sql
SELECT
  JSON_QUERY(json_string, "$.address.post_code") as post_code
FROM `sandbox-aucamp-e332.dailev.json_string`
LIMIT 1000
```

```
## Structured
poetry run coopnorge.demo2206.gcp avro-schema | tee /dev/stderr > avro-schema.json
bq rm sandbox-aucamp-e332:dailev.structured
bq load --project_id=sandbox-aucamp-e332 --source_format=NEWLINE_DELIMITED_JSON sandbox-aucamp-e332:dailev.structured data.ndjson avro-schema.json
```

```sql
SELECT address.post_code
FROM `sandbox-aucamp-e332.dailev.structured`
LIMIT 1000
```


