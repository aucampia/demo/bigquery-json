from dataclasses import dataclass, field

from datetime import date, datetime
import random
from typing import Any, Dict, Optional, Set, Type, cast
import enum
from pydantic import BaseModel

import string

from .faker import fake
from faker import Faker
from pydantic_avro.base import AvroBase  # type: ignore[import]


def pydantic_to_avro(model: Type[BaseModel], by_alias: bool = True) -> Dict[str, Any]:
    return cast(Dict[str, Any], AvroBase._avro_schema(model.schema(by_alias)))


class CommunicationChannel(str, enum.Enum):
    EMAIL = "email"
    PHONE = "phone"


class PostalAddress(BaseModel):
    country_code: str
    country: str
    city: str
    post_code: str
    street_name: str
    steeet_number: str

    @classmethod
    def fake(cls, fake: Faker = fake) -> "PostalAddress":
        return cls(
            country_code=fake.current_country_code(),
            country=fake.current_country_code(),
            city=fake.city(),
            post_code=fake.postcode(),
            street_name=fake.street_name(),
            steeet_number=fake.random_int(min=0, max=100),
        )


COOP_ID_LENGTH = 10
PHONE_NUMBER_LENGTH = 8


def random_bool(probability: float) -> bool:
    return random.random() < probability


class CustomerProfile(BaseModel):
    coop_id: str
    joined_date: date
    birth_date: date
    death_date: Optional[date]
    email: str
    phone_number: str
    address: PostalAddress

    @classmethod
    def fake(cls, fake: Faker = fake) -> "CustomerProfile":
        return cls(
            coop_id="".join(random.choices(string.digits, k=COOP_ID_LENGTH)),
            joined_date=fake.date_between(start_date="-10y", end_date="now"),
            birth_date=fake.date_between(start_date="-50y", end_date="-18y"),
            death_date=fake.date_between(start_date="-50y", end_date="-18y")
            if random_bool(0.05)
            else None,
            email=fake.email(),
            phone_number="+47"
            + "".join(random.choices(string.digits, k=PHONE_NUMBER_LENGTH)),
            address=PostalAddress.fake(),
        )
