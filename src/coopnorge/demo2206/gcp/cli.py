#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=88 cc=+1:
# vim: set filetype=python tw=88 cc=+1:

import csv
from io import TextIOWrapper
import json
import logging
import os
from pathlib import Path, PurePath
import sys
from typing import BinaryIO, Optional
from urllib.parse import urlsplit

import typer
from coopnorge.demo2206.gcp.models import CustomerProfile, pydantic_to_avro
import fsspec  # type: ignore[import]


logger = logging.getLogger(__name__)

"""
https://click.palletsprojects.com/en/7.x/api/#parameters
https://click.palletsprojects.com/en/7.x/options/
https://click.palletsprojects.com/en/7.x/arguments/
https://typer.tiangolo.com/
https://typer.tiangolo.com/tutorial/options/
"""


cli = typer.Typer()


@cli.callback()
def cli_callback(
    ctx: typer.Context, verbosity: int = typer.Option(0, "--verbose", "-v", count=True)
) -> None:
    if verbosity is not None:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, verbosity)) * 10
            - min(max(0, verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)

    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    logger.debug(
        "logging.level = %s, LOGGER.level = %s",
        logging.getLogger("").getEffectiveLevel(),
        logger.getEffectiveLevel(),
    )


@cli.command("generate")
def cli_generate(
    ctx: typer.Context,
    output: str = typer.Option(..., "--output", "-o"),
    format: Optional[str] = typer.Option(..., "--format", "-f"),
    record_count: int = typer.Option(1000),
) -> None:

    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    if len(urlsplit(output).scheme) <= 1:
        output = Path(output).absolute().as_uri()

    bio: BinaryIO
    if format == "ndjson":
        with fsspec.open(output, "wb", auto_mkdir=True) as bio:
            tio = TextIOWrapper(bio, encoding="utf-8")
            for index in range(record_count):
                profile = CustomerProfile.fake()
                tio.write(profile.json())
                tio.write("\n")
    elif format == "csv":
        with fsspec.open(output, "wb", auto_mkdir=True) as bio:
            tio = TextIOWrapper(bio, encoding="utf-8")
            csvwriter = csv.writer(tio)
            for index in range(record_count):
                profile = CustomerProfile.fake()
                csvwriter.writerow([profile.json()])
    else:
        raise ValueError(f"unsupported format {format}, must be csv or ndjson")


@cli.command("avro-schema")
def cli_avro_schema(
    ctx: typer.Context
) -> None:

    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    avro_schema = pydantic_to_avro(CustomerProfile)
    json.dump(avro_schema["fields"], sys.stdout, sort_keys=True, indent="  ")


def main() -> None:
    logging.basicConfig(
        level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s.%(msecs)03d %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )

    cli()


if __name__ == "__main__":
    main()
