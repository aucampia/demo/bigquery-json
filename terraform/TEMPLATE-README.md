# terraform template

## Using

```bash
## Initial sync
rsync -av --no-owner --no-group --no-times --checksum --no-perms --ignore-existing \
    --exclude={.git,TEMPLATE-*.md,.terraform,.terraform.lock.hcl} \
    ~/sw/d/gitlab.com/aucampia/templatesx/terraform/ ./ --dry-run

## Diff summary ...
diff -u -r -q \
    --exclude={.git,TEMPLATE-*.md,.terraform,.terraform.lock.hcl} \
    ~/sw/d/gitlab.com/aucampia/templatesx/terraform/ ./

## vimdiff
diff -u -r \
    --exclude={.git,TEMPLATE-*.md,.terraform,.terraform.lock.hcl} \
    ~/sw/d/gitlab.com/aucampia/templatesx/terraform/ ./ \
    | sed -E -n 's,^diff.* /,vimdiff /,gp'

## diff
diff -u -r \
    --exclude={.git,TEMPLATE-*.md,.terraform,.terraform.lock.hcl} \
    ~/sw/d/gitlab.com/aucampia/templatesx/terraform/ ./
```
