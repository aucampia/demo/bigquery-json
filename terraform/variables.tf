# https://www.terraform.io/docs/language/values/variables.html

variable "project_name" {
  type = string
}

variable "project_id" {
  type        = string
  description = "Project ID"
}

variable "region" {
  type        = string
  description = "default region for resources"
}

variable "zone" {
  type        = string
  description = "default zone for resources"
}

variable "environment" {
  type = string
}
