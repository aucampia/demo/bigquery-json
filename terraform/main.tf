resource "null_resource" "none" {
}

resource "google_bigquery_dataset" "dailev" {
  dataset_id = "dailev"
  location   = local.bigquery_location
}

resource "google_storage_bucket" "static-site" {
  name          = "coopnorge-${var.project_name}-${var.environment}-files"
  location      = local.gcs_location
  force_destroy = true
  uniform_bucket_level_access = true
}
